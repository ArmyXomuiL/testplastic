//
//  ViewController.swift
//  PhotoEditor
//
//  Created by Саня on 01.04.17.
//  Copyright © 2017 Jim. All rights reserved.
//

import UIKit
//import BCMeshTransformView

class ViewController: UIViewController {
  var model = EditModel()
  
  var meshView: BCMeshTransformView?
  
  var resultImage = UIImageView()
  var editingView: UIImageView!
  
  var startX: CGFloat = 0
  var startY: CGFloat = 0
  var stopX: CGFloat = 0
  var stopY: CGFloat = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let image = UIImage(named: "img1")!
    
    editingView = UIImageView(frame: CGRect(x: 0, y: 0, width: CGFloat(image.size.width), height: CGFloat(image.size.height)))
    editingView.image = image
    editingView.center = view.center
    
    meshView = BCMeshTransformView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
    meshView?.contentView?.addSubview(editingView)
    
    view.addSubview(meshView!)
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let theTouch = touches.first {
      let startPoint = theTouch.location(in: self.view)
      startX = startPoint.x
      startY = startPoint.y
      
//      addPointView(frame: CGRect(x: startX - 4, y: startY - 4, width: 8, height: 8), color: .green)
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    let image = meshView?.getSnapshotImage()
    editingView = UIImageView(frame: CGRect(x: 0, y: 0, width: CGFloat((image?.size.width)!), height: CGFloat((image?.size.height)!)))
    editingView.image = image
    
    meshView?.meshTransform = BCMeshTransform()
    
    for view in (meshView?.contentView.subviews)! {
      view.removeFromSuperview()
    }
    
    meshView?.contentView.addSubview(editingView)
    
    
    
    
    
    let location = touches.first!.location(in: view)
    stopX = location.x
    stopY = location.y
    
    let startPoint = CGPoint(x: startX, y: startY)
    let endPoint = CGPoint(x: stopX, y: stopY)
    
    meshView?.meshTransform = Transforms.simpleMesh(from: startPoint, to: endPoint, in: meshView!)
    
    
    
    
    
    
    
    
//    model = EditManager.prepareEditModel(from: startPoint, to: endPoint, in: editingView)
    
    
    
//    print(editModel.startPoint)
//    print(editModel.endPoint)
//    print(editModel.fullLength)
//    print(editModel.intersectionPoint)
//    print(editModel.angle)
//    print("")
    
//    addPointView(frame: CGRect(x: (model.intersectionPoint?.x)! - 4, y: (model.intersectionPoint?.y)! - 4, width: 8, height: 8), color: .red)
    
//    meshView?.meshTransform = Transforms.buldgeMeshTransform(at: CGPoint(x: 10, y: 10), with: 5, and: CGSize(width: 10, height: 10))
//    meshView?.meshTransform = Transforms.curtainMeshTransform(at: CGPoint(x: 100, y: 100), and: (meshView?.bounds.size)!)
//    meshView?.meshTransform = Transforms.curtainMeshTransform(at: model.intersectionPoint!, and: (editingView?.bounds.size)!)
    
//    meshView?.meshTransform = Transforms.simpleMesh(from: CGPoint(x: 0.2, y: 0.4), to: CGPoint(x: 0.6, y: 0.8))
    
  
    
//    editingView.image = meshView?.getSnapshotImage()//UIImage.imageWithView(view: meshView!, in: editingView!.bounds)
    
    
    
    
    
    
    
//    let image = meshView?.getSnapshotImage()
//
//    editingView.removeFromSuperview()
    
//    editingView = UIImageView(frame: CGRect(x: 0, y: 0, width: CGFloat((image?.cgImage!.width)!), height: CGFloat((image?.cgImage!.height)!)))
//    editingView.image = image
//    editingView.center = view.center
    
//    meshView?.contentView?.addSubview(editingView)
    
//    meshView?.contentView
    
    
    
    
    
//    let gestureVector = GestureVector()
//    let point = gestureVector.calculatePointOfIntersection(startPoint: startPoint, endPoint: endPoint, editingViewFrame: editingView.frame)
    
//    if let point = point {
//      addPointView(frame: CGRect(x: point.x - 4, y: point.y - 4, width: 8, height: 8), color: .red)
//    }
//
//    model = gestureVector.calculatePointOfIntersectionVector(startPoint: startPoint, endPoint: endPoint, editingViewFrame: editingView.frame)
//    if let pointOfIntersection = model.pointOfIntersection {
//      print(model.isVectorToImage)
//      transformImage(origin: pointOfIntersection)
//    }
  }
  
  //MARK: - Actions
  
  @IBAction func applyButtonAction(_ sender: UIButton) {
//    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
//    imageView.image = editingView.image
//    view.addSubview(imageView)
//    let imageRect = editingView.calculateRectOfImageInImageView()
//    
//    var rect = CGRect(x: (model.intersectionPoint?.x)! - 25 - imageRect.origin.x, y: (model.intersectionPoint?.y)! - 25 - imageRect.origin.y, width: 50, height: 50)
//    
//    //Проверем, чтоб не выходило за экран, делаем рект для относительных значений
//    rect.origin.x = rect.origin.x > 0 ? rect.origin.x : 0
//    rect.origin.y = rect.origin.y > 0 ? rect.origin.y : 0
//    
//    rect.size.width = rect.origin.x + rect.size.width < editingView.frame.width ? rect.size.width : editingView.frame.maxX - rect.origin.x - imageRect.origin.x
//    rect.size.height = rect.origin.y + rect.size.height < editingView.frame.height ? rect.size.height : editingView.frame.maxY - rect.origin.y - imageRect.origin.y
//    
//    //Вырезаем
//    var image = editingView.image?.crop(rect: rect)
//    
//    //Корректируем рект на асболютные значения
//    rect.origin.x += imageRect.origin.x
//    rect.origin.y += imageRect.origin.y
//    
//    let originPointInCropped = CGPoint(x: (model.intersectionPoint?.x)! - rect.origin.x, y: (model.intersectionPoint?.y)! - rect.origin.y)
////    image = transform(image: image!, with: originPointInCropped, in: CGRect(x: 0, y: 0, width: 50, height: 50))
//    
////    rect.origin.x = 0
////    rect.origin.y = 0
////    resultImage = UIImageView(frame: rect)
//    
//    resultImage = UIImageView(frame: rect)
//    resultImage.image = image
////    resultImage.backgroundColor = UIColor.red
////    resultImage.contentMode = .scaleToFill
//    
//    view.addSubview(resultImage)
  }
  
  //MARK: - Private
  
//  func transform(image: UIImage, with origin: CGPoint, in rect: CGRect) -> UIImage? {
//    print(image)
//    print(origin)
////    let aaa = bumpDistortionLinear(CGPoint(x: 0, y: 0), radius: 1, angle: (model.angle?.radians)! + Float.pi / 2, scale: 0)
//    let aaa = bumpDistortionLinear(origin, radius: 100, angle: (model.angle?.radians)! + Float.pi / 2, scale: 0)
//  
//    if let cgImage = image.cgImage {
//      let ciImage = CIImage(cgImage: cgImage)
//      
//      let convertedImage = aaa(ciImage)
//      
//      let ciContext = CIContext()
//      let cgImageRef = ciContext.createCGImage(convertedImage, from: rect)
//      return UIImage(cgImage: cgImageRef!)
//    }
//    
//    return nil
//  }
//  
//  func addPointView(frame: CGRect, color: UIColor) {
//    let pointView = UIView(frame: frame)
//    pointView.backgroundColor = color
//    pointView.layer.cornerRadius = 4
//    view.addSubview(pointView)
//  }
}

