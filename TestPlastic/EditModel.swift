//
//  GestureModel.swift
//  PhotoEditor
//
//  Created by Саня on 01.04.17.
//  Copyright © 2017 Jim. All rights reserved.
//

import UIKit

class EditModel: NSObject {
  var startPoint: CGPoint?
  var endPoint:   CGPoint?
  var fullLength: CGFloat?
  
  var intersectionPoint: CGPoint?     //Точка входа вектора в контент изображения
  
  var pointOfEditing: CGPoint?
  var editingLength:  CGFloat?
  
  var angle:                Float?
  
//  var isVectorToImage:      Bool?
//  var vectorLength:         CGFloat?
//  var distanceToImage:      CGFloat?
//  var pointOfIntersection:  CGPoint?
}
