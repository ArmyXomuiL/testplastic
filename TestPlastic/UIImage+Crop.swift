//
//  UIImage+Crop.swift
//  PhotoEditor
//
//  Created by Дмитрий Пономарев on 15.05.17.
//  Copyright © 2017 Jim. All rights reserved.
//

import UIKit

//extension UIImage {
//  func crop(rect: CGRect) -> UIImage {
//    var rect = rect
////    rect.origin.x     *= self.scale
////    rect.origin.y     *= self.scale
////    rect.size.width   *= self.scale
////    rect.size.height  *= self.scale
//    
//    let imageRef = self.cgImage!.cropping(to: rect)
//    let image = UIImage(cgImage: imageRef!, scale: 10, orientation: self.imageOrientation)
//    return image
//  }
//}
//
//extension UIImage {
//  class func imageWithView(view: UIView, in frame: CGRect) -> UIImage {
//  
//    UIGraphicsBeginImageContextWithOptions(frame.size, view.isOpaque, 0.0)
//    view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
//    let img = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//    return img!
//  }
//}

extension UIView {
  public func getSnapshotImage() -> UIImage {
    let rect = self.bounds
//    let rect = CGRect(x: 0, y: 0, width: 200, height: 200)
    
    UIGraphicsBeginImageContextWithOptions(rect.size, self.isOpaque, 0)
    self.drawHierarchy(in: rect, afterScreenUpdates: true)
    let snapshotImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return snapshotImage
  }
}
