//
//  Transforms.swift
//  PhotoEditor
//
//  Created by Дмитрий Пономарев on 26.05.17.
//  Copyright © 2017 Jim. All rights reserved.
//

import UIKit
//import BCMeshTransformView

class Transforms: NSObject {
  class func simpleMesh(from startPoint: CGPoint, to endPoint: CGPoint, in meshView: UIView) -> BCMeshTransform {
    let transform = BCMutableMeshTransform.identityMeshTransform(withNumberOfRows: UInt(meshView.frame.size.height / 10), numberOfColumns: UInt(meshView.frame.size.width / 10))
    
//    let vertex = BCMeshVertex(from: startPoint, to: BCPoint3D(x: endPoint.x, y: endPoint.y, z: 0))
//    transform?.add(vertex)
    
    let startPointXPercent = startPoint.x / meshView.frame.size.width
    let startPointYPercent = startPoint.y / meshView.frame.size.height
    
    let endPointXPercent = endPoint.x / meshView.frame.size.width
    let endPointYPercent = endPoint.y / meshView.frame.size.height
    
    let length = sqrt(pow(startPointXPercent - endPointXPercent, 2) + pow(startPointYPercent - endPointYPercent, 2))
    let cosa = (endPointXPercent - startPointXPercent) / length
    let sina = (endPointYPercent - startPointYPercent) / length
//    let equetionOfLineParameters = equationOfLine(for: CGPoint(x: startPointXPercent, y: startPointYPercent), and: CGPoint(x: endPointXPercent, y: endPointYPercent))
    
//    print(startPointXPercent)
//    print(startPointYPercent)
//    print(endPointXPercent)
//    print(endPointYPercent)
//    print(length)
    
    transform?.mapVertices({ (vertex, vertexIndex) -> BCMeshVertex in
      var vertex = vertex
      
      //удаленность от стартовой точки
      var currentLength = sqrt(pow(startPointXPercent - vertex.from.x, 2) + pow(startPointYPercent - vertex.from.y, 2))
      
      if currentLength <= length {
        currentLength = length - currentLength
        
        
        let force: CGFloat = currentLength / length
//        let currentEndX = cosa * currentLength / exp(force) + vertex.from.x//startPointXPercent
        let currentEndX = cosa * currentLength / exp(force) * 1.5 + vertex.from.x //startPointXPercent
//        let currentEndY = calculateYInLineEquation(for: currentEndX, and: (k: equetionOfLineParameters.k, b: equetionOfLineParameters.b))
        let currentEndY = sina * currentLength / exp(force) + vertex.from.y
        
        vertex.to.x = currentEndX
        vertex.to.y = currentEndY
        
        print(vertex.from)
        print(vertex.to)
      }
      
      return vertex
    })
    
    return transform!
  }
}
